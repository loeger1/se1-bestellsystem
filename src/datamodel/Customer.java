package datamodel;

public class Customer {
	private final String id;
	private String firstName;
	private String lastName;
	private String contact;
	
	protected Customer ( String id, String fullName, String contact ) {
		this.id = id;
		this.lastName = fullName;
		if (fullName == null) {
			this.lastName = "";
		}
		this.firstName = "";
		this.contact = contact;
		if (contact == null) {
			this.contact = "";
		}
	}
	
	public String getId () {
		return id;
	}
	
	public String getFirstName () {
		return firstName;
	}
	
	public void setFirstName ( String firstName) {
		this.firstName = firstName;
		if (firstName == null) {
			this.firstName = "";
		}
	}
	
	public String getLastName () {
		return lastName;
	}
	
	public void setLastName ( String lastName) {
		this.lastName = lastName;
		if (lastName == null) {
			this.lastName = "";
		}
	}
	
//	public String getName () {
//		return firstName + lastName;
//	}
//	
//	public void setName ( String fullName ) {
//		if ( fullName.contains(",")) {
//			this.lastName = fullName.split(", ")[0];
//			this.firstName = fullName.split(", ") [1];
//		} else {
//			int index = fullName.lastIndexOf(" ");
//			this.firstName = fullName.substring( 0, index );
//			this.lastName = fullName.substring( index + 1 );
//		}
//	}
	
	public String getContact () {
		return contact;
	}
	
	public void setContact ( String contact) {
		this.contact = contact;
		if (contact == null) {
			this.contact = "";
		}
	}
}
