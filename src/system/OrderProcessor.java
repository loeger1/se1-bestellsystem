package system;

import java.util.function.Consumer;

import datamodel.Order;
import datamodel.OrderItem;

final class OrderProcessor implements Components.OrderProcessor{
	
	
	public OrderProcessor(system.Components.InventoryManager inventoryManager) {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public boolean accept(Order order) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean accept(Order order, Consumer<Order> acceptCode, Consumer<Order> rejectCode,
			Consumer<OrderItem> rejectedOrderItemCode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public long orderValue(Order order) {
		// TODO Auto-generated method stub
		return 0;
	}
	

	public long vat(long grossValue) {
		return grossValue*100/119*19/100;
	}

	@Override
	public long vat(long grossValue, int rateIndex) {
		if (rateIndex == 1) {
			return grossValue*100/119*19/100;
		}
		if (rateIndex == 2) {
			return grossValue*100/107*7/100;
		}
		return 0;
	}

}
