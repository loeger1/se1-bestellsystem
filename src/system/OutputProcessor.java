package system;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import datamodel.Customer;
import datamodel.Order;
import datamodel.OrderItem;
import system.Components.InventoryManager;
import system.Components.OrderProcessor;

final class OutputProcessor implements Components.OutputProcessor {
	
	private final int printLineWidth = 95;
	private final Components.OrderProcessor orderProcessor;
	

	public OutputProcessor(InventoryManager inventoryManager, OrderProcessor orderProcessor) {
		this.orderProcessor = orderProcessor;
	}

	@Override
	public void printOrders(List<Order> orders, boolean printVAT) {
		
		StringBuffer sbAllOrders = new StringBuffer( "-------------" );
		StringBuffer sbLineItem = new StringBuffer();


		/*
		 * Insert code to print orders with all order items:
		 */
		long overall = 0;
		long overallVAT = 0;
		
		for (Order o: orders) {
			long overallOrder = 0;
			long orderVAT = 0;
			Customer customer = o.getCustomer();
			String customerName = splitName( customer, customer.getFirstName() + " " + customer.getLastName() );
			sbLineItem.append( "#" + o.getId() + ", " + customerName + "'s Bestellung: " );
			for (OrderItem oI: o.getItems()) {
				sbLineItem.append( oI.getUnitsOrdered() + "x " + oI.getDescription() + ", " );
				overallOrder = overallOrder + ( oI.getUnitsOrdered() * oI.getArticle().getUnitPrice() );
			}
			orderVAT = orderProcessor.vat(overallOrder);
			sbLineItem.setLength( sbLineItem.length() - 2 );
			String fmtPrice1 = fmtPrice( overallOrder, "EUR", 14 );
			String left = sbLineItem.toString();
			sbLineItem = fmtLine( left, fmtPrice1, printLineWidth );
			sbAllOrders.append( "\n" );
			sbAllOrders.append( sbLineItem );
			sbLineItem.delete(0, sbLineItem.length());
			overall = overall + overallOrder;
			overallVAT = overallVAT + orderVAT;
    	}

		// calculate total price
		String fmtPriceTotal = pad( fmtPrice( overall, "", " EUR" ), 14, true );
		
		// calculate total VAT
		String fmtPriceVAT = pad( fmtPrice( overallVAT, "", " EUR" ), 14, true );

		// append final line with totals
		sbAllOrders.append( "\n" )
			.append( fmtLine( "-------------", "-------------", printLineWidth ) )
			.append( "\n" )
			.append( fmtLine( "Gesamtwert aller Bestellungen:", fmtPriceTotal, printLineWidth ) );
		
		// append line with VAT
		
		if (printVAT == true) {
			sbAllOrders.append( "\n" )
			.append( fmtLine( "Im Gesamtbetrag enthaltene Mehrwertsteuer (19%):", fmtPriceVAT, printLineWidth ) );
		}
		
		// print sbAllOrders StringBuffer with all output to System.out
		System.out.println( sbAllOrders.toString() );
		
	}

	@Override
	public void printInventory() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String fmtPrice(long price, String currency) {
		String fmtPrice = pad( fmtPrice( price, "", " " + currency ), 14, true );
		return fmtPrice;
	}

	@Override
	public String fmtPrice(long price, String currency, int width) {
		String fmtPrice = pad( fmtPrice( price, "", " " + currency ), 14, true );
		return fmtPrice;
	}
	

	private String fmtPrice( long price, String prefix, String postfix ) {
		StringBuffer fmtPriceSB = new StringBuffer();
		if( prefix != null ) {
			fmtPriceSB.append( prefix );
		}

		fmtPriceSB = fmtPrice( fmtPriceSB, price );

		if( postfix != null ) {
			fmtPriceSB.append( postfix );
		}
		return fmtPriceSB.toString();
	}
	
	private StringBuffer fmtPrice( StringBuffer sb, long price ) {
		if( sb == null ) {
			sb = new StringBuffer();
		}
		double dblPrice = ( (double)price ) / 100.0;			// convert cent to Euro
		DecimalFormat df = new DecimalFormat( "#,##0.00",
			new DecimalFormatSymbols( new Locale( "de" ) ) );	// rounds double to 2 digits

		String fmtPrice = df.format( dblPrice );				// convert result to String in DecimalFormat
		sb.append( fmtPrice );
		return sb;
	}
	
	private String pad( String str, int width, boolean rightAligned ) {
		String fmtter = ( rightAligned? "%" : "%-" ) + width + "s";
		String padded = String.format( fmtter, str );
		return padded;
	}

	@Override
	public StringBuffer fmtLine(String leftStr, String rightStr, int width) {
		StringBuffer sb = new StringBuffer( leftStr );
		int shiftable = 0;		// leading spaces before first digit
		for( int i=1; rightStr.charAt( i ) == ' ' && i < rightStr.length(); i++ ) {
			shiftable++;
		}
		final int tab1 = width - rightStr.length() + 1;	// - ( seperator? 3 : 0 );
		int sbLen = sb.length();
		int excess = sbLen - tab1 + 1;
		int shift2 = excess - Math.max( 0, excess - shiftable );
		if( shift2 > 0 ) {
			rightStr = rightStr.substring( shift2, rightStr.length() );
			excess -= shift2;
		}
		if( excess > 0 ) {
			switch( excess ) {
			case 1:	sb.delete( sbLen - excess, sbLen ); break;
			case 2: sb.delete( sbLen - excess - 2 , sbLen ); sb.append( ".." ); break;
			default: sb.delete( sbLen - excess - 3, sbLen ); sb.append( "..." ); break;
			}
		}
		String strLineItem = String.format( "%-" + ( tab1 - 1 ) + "s%s", sb.toString(), rightStr );
		sb.setLength( 0 );
		sb.append( strLineItem );
		return sb;
	}

	@Override
	public String splitName(Customer customer, String name) {
		if ( name.contains(",")) {
			customer.setLastName(name.split(", ")[0 ].trim());
			customer.setFirstName(name.split(", ")[1]);
		} else {
			int index = name.lastIndexOf(" ");
			customer.setFirstName(name.substring( 0, index ).trim());
			customer.setLastName(name.substring( index + 1 ).trim());
		}
		
		return singleName(customer);
	}

	@Override
	public String singleName(Customer customer) {
		return customer.getLastName() + ", " + customer.getFirstName();
	}
	
}
